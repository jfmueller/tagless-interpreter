﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Language
{
    /// <summary>
    /// Nothing
    /// </summary>
    public sealed class Nothing : RecipeType
    {
    }
}
