﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Language
{
    /// <summary>
    /// An EDSL for recipes
    /// </summary>
    /// <typeparam name="T">The type of this term</typeparam>
    /// <typeparam name="I">The result type of the interpreter</typeparam>
    public interface IRecipeTerm<T, I> 
    {
        #region Lambda Calculus
        /// <summary>
        /// A constant value
        /// </summary>
        /// <typeparam name="Q"></typeparam>
        /// <param name="theValue"></param>
        /// <returns></returns>
        IRecipeTerm<Q, I> Constant<Q>(Q theValue);

        /// <summary>
        /// Lambda abstraction (ie a function) with one parameter
        /// </summary>
        /// <typeparam name="Q">Parameter type of the function</typeparam>
        /// <typeparam name="R">Result type of the function</typeparam>
        /// <param name="variable">Identifier for the variable. <remarks>The identifier does not matter wrt interpretation</remarks></param>
        /// <param name="theFunction"></param>
        /// <returns></returns>
        IRecipeTerm<Func<Q, R>, I> Lambda<Q, R>(String variable, Expression<Func<IRecipeTerm<Q, I>, IRecipeTerm<R, I>>> theFunction);

        /// <summary>
        /// Apply a function to a value
        /// </summary>
        /// <typeparam name="Q"></typeparam>
        /// <typeparam name="R"></typeparam>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        IRecipeTerm<R, I> Apply<Q, R>(IRecipeTerm<Func<Q, R>, I> left, IRecipeTerm<Q, I> right);

        /// <summary>
        /// Declare a function/variable reference.
        /// </summary>
        /// <typeparam name="Q"></typeparam>
        /// <typeparam name="R"></typeparam>
        /// <param name="name"></param>
        /// <param name="let"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        IRecipeTerm<R, I> Let<Q, R>(String name, IRecipeTerm<Q, I> let, Func<IRecipeTerm<Q, I>, IRecipeTerm<R, I>> body);

        /// <summary>
        /// Declare a recursive function
        /// </summary>
        /// <typeparam name="Q"></typeparam>
        /// <typeparam name="R"></typeparam>
        /// <param name="name"></param>
        /// <param name="let"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        IRecipeTerm<R, I> LetRec<Q, R>(String name, Func<Func<IRecipeTerm<Q, I>, IRecipeTerm<Q, I>>, IRecipeTerm<Q, I>> let, Func<IRecipeTerm<Q, I>, IRecipeTerm<Q, I>> body);
        #endregion

        #region Recipe operators
        #endregion

        /// <summary>
        /// Evaluate the term
        /// </summary>
        /// <returns></returns>
        I Interpret();
    }
}
