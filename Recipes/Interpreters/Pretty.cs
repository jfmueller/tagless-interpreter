﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Recipes.Language;

namespace Recipes.Interpreters
{
    public static class Pretty
    {
        public static Pretty<Nothing> Initialise()
        {
            return new Pretty<Nothing>(String.Empty);
        }
    }

    public sealed class Pretty<I> : IRecipeTerm<I, string>
    {
        private readonly String value;

        internal Pretty(string val)
        {
            this.value = val;
        }

        private IRecipeTerm<Q, String> Variable<Q>(string varName)
        {
            return new Pretty<Q>(varName);
        }

        public IRecipeTerm<Q, string> Constant<Q>(Q theValue) 
        {
            return new Pretty<Q>(String.Format("({0})", value));
        }

        public IRecipeTerm<Func<Q, R>, string> Lambda<Q, R>(string variable, System.Linq.Expressions.Expression<Func<IRecipeTerm<Q, string>, IRecipeTerm<R, string>>> theFunction)
        {
            var theVariable = this.Variable<Q>(variable);
            var comp = theFunction.Compile();
            return new Pretty<Func<Q, R>>(String.Format("λ{0}.({1})", variable, comp(theVariable).Interpret()));
        }

        public IRecipeTerm<R, string> Apply<Q, R>(IRecipeTerm<Func<Q, R>, string> left, IRecipeTerm<Q, string> right)
        {
            return new Pretty<R>(String.Format("({0}) ({1})", left.Interpret(), right.Interpret()));
        }

        public string Interpret()
        {
            return value;
        }

        public IRecipeTerm<R, string> Let<Q, R>(string name, IRecipeTerm<Q, string> let, Func<IRecipeTerm<Q, string>, IRecipeTerm<R, string>> body)
        {
            return new Pretty<R>(String.Format("let {0}={1} in{2}{3}",
                name,
                let.Interpret(),
                System.Environment.NewLine,
                body(this.Variable<Q>(name))));
        }

        public IRecipeTerm<R, string> LetRec<Q, R>(string name, Func<Func<IRecipeTerm<Q, string>, IRecipeTerm<Q, string>>, IRecipeTerm<Q, string>> let, Func<IRecipeTerm<Q, string>, IRecipeTerm<Q, string>> body)
        {
            var theVariable = this.Variable<Q>(name);
            Func<IRecipeTerm<Q, string>, IRecipeTerm<Q, string>> recF = null;
            recF = x => let(recF);
            return new Pretty<R>(String.Format("let {0}={1} in{2}{3}",
                name,
                let(recF).Interpret(),
                System.Environment.NewLine,
                body(this.Variable<Q>(name))));
        }
    }

}
