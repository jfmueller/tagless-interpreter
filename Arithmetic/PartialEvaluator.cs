﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arithmetic
{
    public sealed class PartialEvaluator<T>
    {
        //The partial evaluator should keep track of known results using a ``known''
        //field
        bool IsKnown { get; set; }

        T Value { get; set; }
    }
}
