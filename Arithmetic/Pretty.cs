﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Arithmetic
{
    public static class Pretty
    {
        private sealed class PrettyImpl<I> : ITerm<I, String>
        {
            private readonly string val;

            internal PrettyImpl(string v)
            {
                this.val = v;
            }

            public string Interpret()
            {
                return val;
            }

            public ITerm<int, string> Constant(int theValue)
            {
                return new PrettyImpl<int>(theValue.ToString());
            }

            public ITerm<int, string> Add(ITerm<int, string> left, ITerm<int, string> right)
            {
                return new PrettyImpl<int>(String.Format("+ {0} {1}",
                    left.Interpret(),
                    right.Interpret()));
            }

            public ITerm<int, string> Multiply(ITerm<int, string> left, ITerm<int, string> right)
            {
                return new PrettyImpl<int>(String.Format("* {0} {1}",
                    left.Interpret(),
                    right.Interpret()));
            }

            public Func<ITerm<Q, string>, ITerm<R, string>> Lambda<Q, R>(Expression<Func<ITerm<Q, string>, ITerm<R, string>>> theFunction)
            {
                // Extract the variable name from the expression
                var theVariable = theFunction.Parameters.First().Name;
                var variable = new PrettyImpl<Q>(theVariable);
                return x => new PrettyImpl<R>(String.Format(
                    "(\\{0} -> {1})", 
                    variable.Interpret(), 
                    theFunction.Compile()(variable).Interpret()));
            }

            public ITerm<R, string> Apply<Q, R>(Func<ITerm<Q, string>, ITerm<R, string>> function, ITerm<Q, string> argument)
            {
                return new PrettyImpl<R>(String.Format("{0}({1})", function(argument).Interpret(), argument.Interpret())); 
            }

            public ITerm<int, string> IfZero(ITerm<int, string> term, ITerm<int, string> then, ITerm<int, string> ellse)
            {
                return new PrettyImpl<int>(String.Format("(if-zero ({0}) then ({1}) else ({2}))", 
                    term.Interpret(),
                    then.Interpret(), 
                    ellse.Interpret()));
            }

            public ITerm<S, string> Let<Q, R, S>(Func<ITerm<Q, string>, ITerm<R, string>> theFunction, Expression<Func<Func<ITerm<Q, string>, ITerm<R, string>>, ITerm<S, string>>> theScope)
            {
                //function name is given as a parameter in the expression
                var funcName = theScope.Parameters.Aggregate(
                    new StringBuilder(),
                    (s, p) => s.Append(p.Name)).ToString();
                var variable = new PrettyImpl<Q>(String.Empty);//Not pretty
                Func<ITerm<Q, string>, ITerm<R, string>> substFunc = x => new PrettyImpl<R>(funcName);
                return new PrettyImpl<S>(String.Format("let {0}={1} in {2}",
                    funcName,
                    theFunction(variable).Interpret(),
                    theScope.Compile()(substFunc).Interpret()));
            }
            
            public Func<ITerm<R, string>, ITerm<R, string>> LambdaRec<R>(Expression<Func<ITerm<R, string>, Func<ITerm<R, string>, ITerm<R, string>>, ITerm<R, string>>> theFunction)
            {
                //function name is given as a parameter in the expression
                var funcName = theFunction.Parameters.Skip(1).First().Name;
                var variableName = theFunction.Parameters.First().Name;
                var variable = new PrettyImpl<R>(variableName);//Not pretty
                Func<ITerm<R, string>, ITerm<R, string>> substFunc = x => new PrettyImpl<R>(funcName);
                return x => new PrettyImpl<R>(String.Format("fix (\\{0} \\{1} -> {2})",
                    variableName,
                    funcName,
                    theFunction.Compile()(variable, substFunc).Interpret()));
            }
        }

        public static ITerm<I, String> Initialise<I>()
        {
            return new PrettyImpl<I>(String.Empty);
        }
    }
}
