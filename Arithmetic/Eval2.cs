﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Arithmetic
{
    public static class Eval2
    {
        public static ITerm<I, Expression<Func<int>>> Initialise<I>()
        {
            return new EvalImpl2<I>(null);
        }

        private sealed class EvalImpl2<I> : ITerm<I, Expression<Func<int>>>
        {
            private readonly Expression<Func<int>> value;

            internal EvalImpl2(Expression<Func<int>> theVal)
            {
                this.value = theVal;
            }

            public Expression<Func<int>> Interpret()
            {
                return value;
            }

            public ITerm<int, Expression<Func<int>>> Constant(int theValue)
            {
                return new EvalImpl2<int>(() => theValue);
            }

            public ITerm<int, Expression<Func<int>>> Add(ITerm<int, Expression<Func<int>>> left, ITerm<int, Expression<Func<int>>> right)
            {
                var res = Expression.Add(
                    Expression.Invoke(left.Interpret()),
                    Expression.Invoke(right.Interpret()));
                return new EvalImpl2<int>(Expression.Lambda<Func<int>>(res));
            }

            public ITerm<int, Expression<Func<int>>> Multiply(ITerm<int, Expression<Func<int>>> left, ITerm<int, Expression<Func<int>>> right)
            {
                var res = Expression.Multiply(
                    left.Interpret(),
                    right.Interpret());
                return new EvalImpl2<int>(Expression.Lambda<Func<int>>(res));
            }

            public Func<ITerm<Q, Expression<Func<int>>>, ITerm<R, Expression<Func<int>>>> Lambda<Q, R>(Expression<Func<ITerm<Q, Expression<Func<int>>>, ITerm<R, Expression<Func<int>>>>> theFunction)
            {
                var lmb = Expression.Lambda<Func<ITerm<Q, Expression<Func<int>>>, ITerm<R, Expression<Func<int>>>>>(theFunction.Body, true, theFunction.Parameters);//Tail call optimisation
                return x => new EvalImpl2<R>(Expression.Lambda<Func<int>>(Expression.Invoke(Expression.Call(
                        Expression.Invoke(
                            lmb,
                            Expression.Constant(x)),
                        typeof(ITerm<Q, Expression<Func<int>>>).GetMethod("Interpret")))));
            }

            public ITerm<R, Expression<Func<int>>> Apply<Q, R>(Func<ITerm<Q, Expression<Func<int>>>, ITerm<R, Expression<Func<int>>>> function, ITerm<Q, Expression<Func<int>>> argument)
            {
                return function(argument);
            }

            public ITerm<int, Expression<Func<int>>> IfZero(ITerm<int, Expression<Func<int>>> term, ITerm<int, Expression<Func<int>>> then, ITerm<int, Expression<Func<int>>> ellse)
            {
                return new EvalImpl2<int>(Expression.Lambda<Func<int>>(
                        Expression.IfThenElse(
                        Expression.Equal(
                            Expression.Constant(0),
                            Expression.Invoke(term.Interpret())),
                        Expression.Invoke(then.Interpret()),
                        Expression.Invoke(ellse.Interpret())),
                        true));
            }

            public ITerm<S, Expression<Func<int>>> Let<Q, R, S>(
                Func<ITerm<Q, Expression<Func<int>>>, ITerm<R, Expression<Func<int>>>> theFunction, 
                Expression<Func<Func<ITerm<Q, Expression<Func<int>>>, ITerm<R, Expression<Func<int>>>>, ITerm<S, Expression<Func<int>>>>> theScope)
            {
                Expression<Func<ITerm<Q, Expression<Func<int>>>, ITerm<R, Expression<Func<int>>>>> expr = q => theFunction(q);
                return new EvalImpl2<S>(Expression.Lambda<Func<int>>(Expression.Invoke(Expression.Call(
                        Expression.Invoke(
                            theScope,
                            expr),
                        typeof(ITerm<Q, Expression<Func<int>>>).GetMethod("Interpret"))),
                        true));
            }

            public ITerm<S, Expression<Func<int>>> LetRec<R, S>(
                Expression<Func<ITerm<R, Expression<Func<int>>>, Func<ITerm<R, Expression<Func<int>>>, ITerm<R, Expression<Func<int>>>>, ITerm<R, Expression<Func<int>>>>> theFunction, 
                Expression<Func<Func<ITerm<R, Expression<Func<int>>>, ITerm<R, Expression<Func<int>>>>, ITerm<S, Expression<Func<int>>>>> theScope)
            {
                //theFunction :: (R -> (R -> R)) -> R)
                //theScope :: ((R -> R) -> S)
                //we need to build a lambda expression of type R -> R
                //and then evaluate the scope with it.
                Expression<Func<ITerm<R, Expression<Func<int>>>, ITerm<R, Expression<Func<int>>>>> rec = null;
                rec = Expression.Lambda<Func<ITerm<R, Expression<Func<int>>>, ITerm<R, Expression<Func<int>>>>>(null);
                //var param = Expression.Invoke(
                throw new NotImplementedException();
            }



            public Func<ITerm<R, Expression<Func<int>>>, ITerm<R, Expression<Func<int>>>> LambdaRec<R>(Expression<Func<ITerm<R, Expression<Func<int>>>, Func<ITerm<R, Expression<Func<int>>>, ITerm<R, Expression<Func<int>>>>, ITerm<R, Expression<Func<int>>>>> theFunction)
            {
                throw new NotImplementedException();
            }
        }

    }
}
