﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Arithmetic
{
    public static class Eval
    {
        public static ITerm<I, Func<int>> Initialise<I>()
        {
            return new EvalImpl<I>(() => 0);
        }

        private sealed class EvalImpl<I> : ITerm<I, Func<int>>
        {
            private readonly Func<int> value;

            internal EvalImpl(Func<int> theVal)
            {
                this.value = theVal;
            }

            public Func<int> Interpret()
            {
                return value;
            }

            public ITerm<int, Func<int>> Constant(int theValue)
            {
                return new EvalImpl<int>(() => theValue);
            }

            public ITerm<int, Func<int>> Add(ITerm<int, Func<int>> left, ITerm<int, Func<int>> right)
            {
                return new EvalImpl<int>(
                    () => left.Interpret()() + right.Interpret()());
            }

            public ITerm<int, Func<int>> Multiply(ITerm<int, Func<int>> left, ITerm<int, Func<int>> right)
            {
                return new EvalImpl<int>(() =>
                    left.Interpret()() * right.Interpret()());
            }

            public Func<ITerm<Q, Func<int>>, ITerm<R, Func<int>>> Lambda<Q, R>(Expression<Func<ITerm<Q, Func<int>>, ITerm<R, Func<int>>>> theFunction)
            {
                return theFunction.Compile();
            }

            public Func<ITerm<R, Func<int>>, ITerm<R, Func<int>>> LambdaRec<R>(Expression<Func<ITerm<R, Func<int>>, Func<ITerm<R, Func<int>>, ITerm<R, Func<int>>>, ITerm<R, Func<int>>>> theFunction)
            {
                var f2 = theFunction.Compile();
                return Fix(f2);
            }

            private Func<ITerm<R, Func<int>>, ITerm<R, Func<int>>> Fix<R>(Func<ITerm<R, Func<int>>, Func<ITerm<R, Func<int>>, ITerm<R, Func<int>>>, ITerm<R, Func<int>>> theFunction)
            {
                return x => theFunction(x, Fix(theFunction));
            }

            public ITerm<R, Func<int>> Apply<Q, R>(Func<ITerm<Q, Func<int>>, ITerm<R, Func<int>>> function, ITerm<Q, Func<int>> argument)
            {
                return new EvalImpl<R>(() => function(argument).Interpret()());
            }

            public ITerm<int, Func<int>> IfZero(ITerm<int, Func<int>> term, ITerm<int, Func<int>> then, ITerm<int, Func<int>> ellse)
            {
                return new EvalImpl<int>(() =>
                    term.Interpret()() == 0 ? then.Interpret()() : ellse.Interpret()());
            }

            public ITerm<S, Func<int>> Let<Q, R, S>(Func<ITerm<Q, Func<int>>, ITerm<R, Func<int>>> theFunction, Expression<Func<Func<ITerm<Q, Func<int>>, ITerm<R, Func<int>>>, ITerm<S, Func<int>>>> theScope)
            {
                return new EvalImpl<S>(() => theScope.Compile()(theFunction).Interpret()());
            }


        }
    }
}
