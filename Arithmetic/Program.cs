﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arithmetic
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Interpreter I: Pretty");
            var ini = Pretty.Initialise<int>();
            var pretty = TenPlusTwelve(ini);
            Console.WriteLine(pretty.Interpret());
            Console.WriteLine();

            Console.WriteLine("Interpreter II: Evaluate");
            var ev = Eval.Initialise<int>();
            var eval = TenPlusTwelve(ev);
            Console.WriteLine(eval.Interpret()());
            Console.WriteLine();

            Console.WriteLine("Factorial I: Pretty");
            var fac = Factorial(ini);
            Console.WriteLine(ini.Apply(fac, ini.Constant(10)).Interpret());
            Console.WriteLine();

            Console.WriteLine("Factorial II: Eval");
            var fac2 = Factorial(ev);
            Console.WriteLine(ev.Apply(fac2, ev.Constant(10)).Interpret()());
            Console.WriteLine();
            Console.Read();
        }

        /// <summary>
        /// Add ten and twelve using a function
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="i"></param>
        /// <returns></returns>
        static ITerm<int, T> TenPlusTwelve<T>(ITerm<int, T> i)
        {
            return i.Let(AddTwelve(i),
                adder => i.Apply(adder, i.Constant(10)));
        }

        static Func<ITerm<int, T>, ITerm<int, T>> AddTwelve<T>(ITerm<int, T> i)
        {
            return i.Lambda<int, int>(x => i.Add(x, i.Constant(12)));
        }

        /// <summary>
        /// Calculate the factorial of a number using recursion
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="i"></param>
        /// <returns></returns>
        static Func<ITerm<int, T>, ITerm<int, T>> Factorial<T>(ITerm<int, T> i)
        {
            return i.Lambda<int, int>(num =>
                i.Let<int, int, int>(
                    i.LambdaRec<int>((p, factorial) => i.IfZero(p,
                        i.Constant(1),
                        i.Multiply(
                            p,
                            i.Apply(factorial, i.Add(p, i.Constant(-1)))))),
                    factorial => i.Apply(factorial, num)));
        }

        static Func<ITerm<int, T>, Func<ITerm<int, T>, ITerm<int, T>>> Optimise<T>(Func<ITerm<int, T>, Func<ITerm<int, T>, ITerm<int, T>>> program)
        {
            throw new NotImplementedException();
        }
    }
}
