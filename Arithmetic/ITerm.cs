﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Arithmetic
{
    public interface ITerm<I, T> 
    {
        ITerm<int, T> Constant(int theValue);

        ITerm<int, T> Add(ITerm<int, T> left, ITerm<int, T> right);

        ITerm<int, T> Multiply(ITerm<int, T> left, ITerm<int, T> right);

        Func<ITerm<Q, T>, ITerm<R, T>>Lambda<Q, R>(Expression<Func<ITerm<Q, T>, ITerm<R, T>>> theFunction);

        Func<ITerm<R, T>, ITerm<R, T>> LambdaRec<R>(Expression<Func<ITerm<R, T>, Func<ITerm<R, T>, ITerm<R, T>>, ITerm<R, T>>> theFunction);

        ITerm<R, T> Apply<Q, R>(Func<ITerm<Q, T>, ITerm<R, T>> function, ITerm<Q, T> argument);

        ITerm<int, T> IfZero(ITerm<int, T> term, ITerm<int, T> then, ITerm<int, T> ellse);

        ITerm<S, T> Let<Q, R, S>(Func<ITerm<Q, T>, ITerm<R, T>> theFunction, Expression<Func<Func<ITerm<Q, T>, ITerm<R, T>>, ITerm<S, T>>> theScope);

        T Interpret();
    }
}
