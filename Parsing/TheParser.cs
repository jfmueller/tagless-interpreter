﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parseq;
using Parseq.Combinators;

namespace Parsing
{
    public static class TheParser
    {
        public static Parser<char, String> Identifier()
        {
            return Chars.Letter().Many(1).Select(theLetters =>
                    new String(theLetters.ToArray()));
        }

        public static Parser<char, Unit> Spaces()
        {
            return Chars.Space().Many().Ignore();
        }

        public static Parser<char, Unit> Assignment()
        {
            return Chars.OneOf('=').Ignore();
        }

        public static Parser<char, Unit> Semicolon()
        {
            return Chars.OneOf(';').Ignore();
        }

        public static Parser<char, Int32> Integer()
        {
            return Chars.Number().Many(1).Select(theDigits => Int32.Parse(new String(theDigits.ToArray())));
        }

        /// <summary>
        /// Parse assignments of the form x = 42;
        /// </summary>
        /// <returns></returns>
        public static Parser<char, Tuple<String, Int32>> VariableAssignment()
        {
            return Identifier().Left(Spaces()).Left(Assignment()).Left(Spaces()).SelectMany(id =>
                Integer().SelectMany(integer =>
                    Semicolon().Select(s => Tuple.Create(id, integer))));

        }

        /// <summary>
        /// Parse a list of assignments, separated by spaces:
        /// x=42; y = 2; u = 998;
        /// </summary>
        /// <returns></returns>
        public static Parser<char, IList<Tuple<String, Int32>>> AssignmentList()
        {
            return VariableAssignment().Left(Spaces()).Many(1).Select(theTuples => theTuples.ToList());
        }

        /// <summary>
        /// Parse a list of variable assignments
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static IOption<IList<Tuple<String, Int32>>> Parse(String input)
        {
            var res = AssignmentList().Run(input.AsStream());
            if (res.IsSuccess())
                return res.Left.Value;
            else
                return new Option<IList<Tuple<String, Int32>>>.None();
        }
    }
}
