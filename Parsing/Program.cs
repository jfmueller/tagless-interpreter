﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parsing
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a list of variable assignments, or an empty string if you want to quit.");
            var l = Console.ReadLine();
            if (String.IsNullOrEmpty(l))
                return;

            var theResult = TheParser.Parse(l);
            if (theResult.HasValue)
            {
                var theVal = theResult.Value;
                Console.WriteLine("You entered: ");
                foreach (var assignment in theVal)
                {
                    Console.WriteLine(String.Format("{0} = {1};", assignment.Item1, assignment.Item2));
                }
            }
            else
            {
                Console.WriteLine("Parsing error");
            }
            Main(new String[] {});
        }
    }
}
